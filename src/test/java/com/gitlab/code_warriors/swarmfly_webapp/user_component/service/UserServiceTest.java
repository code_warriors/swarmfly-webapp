package com.gitlab.code_warriors.swarmfly_webapp.user_component.service;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.gitlab.code_warriors.swarmfly_webapp.user_component.entity.UserEntity;
import com.gitlab.code_warriors.swarmfly_webapp.user_component.repository.IUserRepository;
import java.util.Optional;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Test of user service in user component.
 * 
 * @author christian.hueser
 */
@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    /**
     * Mocked user repository.
     */
    @Mock
    private IUserRepository userRepository;

    /**
     * Mocked user service.
     */
    @InjectMocks
    private UserService userService;
    
    /**
     * Set up executed once before all test cases.
     */
    @BeforeAll
    public static void setUpClass() {
    }
    
    /**
     * Tear down executed once after all test cases.
     */
    @AfterAll
    public static void tearDownClass() {
    }
    
    /**
     * Set up executed before each test case.
     */
    @BeforeEach
    public void setUp() {
    }
    
    /**
     * Tear down executed after each test case.
     */
    @AfterEach
    public void tearDown() {
    }

    /**
     * Tests finding user by ID with user service.
     */
    @Test
    public void testFindUserByUserId() {
        Long userId = 1L;
        UserEntity user = new UserEntity();
        user.setId(userId);

        when(userRepository.findById(ArgumentMatchers.any(Long.class))).thenReturn(Optional.of(user));

        Optional<UserEntity> userFound = userService.getUserByUserId(userId);

        assertThat(userFound.get().getUsername()).isSameAs(user.getUsername());
        verify(userRepository).findById(userId);
    }
    
    /**
     * Tests finding user by username with user service.
     */
    @Test
    public void testFindUserByUsername() {
        String username = "codingsmurf";
        UserEntity user = new UserEntity();
        user.setUsername(username);

        when(userRepository.findByUsername(ArgumentMatchers.any(String.class))).thenReturn(Optional.of(user));

        Optional<UserEntity> userFound = userService.getUserByUsername(username);

        assertThat(userFound.get().getUsername()).isSameAs(user.getUsername());
        verify(userRepository).findByUsername(username);
    }
    
    /**
     * Tests finding user by email with user service.
     */
    @Test
    public void testFindUserByEmail() {
        String email = "c_hueser@web.de";
        UserEntity user = new UserEntity();
        user.setEmail(email);

        when(userRepository.findByEmail(ArgumentMatchers.any(String.class))).thenReturn(Optional.of(user));

        Optional<UserEntity> userFound = userService.getUserByEmail(email);

        assertThat(userFound.get().getUsername()).isSameAs(user.getUsername());
        verify(userRepository).findByEmail(email);
    }
    
    /**
     * Tests finding user by email with user service.
     */
    @Test
    public void testIsUserExistingByUsername() {
        String username = "codingsmurf";
        UserEntity user = new UserEntity();
        user.setUsername(username);

        when(userRepository.findByUsername(ArgumentMatchers.any(String.class))).thenReturn(Optional.of(user));

        boolean isUserExisting = userService.isUserExistingByUsername(username);

        assertThat(isUserExisting).isSameAs(true);
        verify(userRepository).findByUsername(username);
    }
    
    /**
     * Tests finding user by email with user service.
     */
    @Test
    public void testIsUserExistingByEmail() {
        String email = "c_hueser@web.de";
        UserEntity user = new UserEntity();
        user.setEmail(email);

        when(userRepository.findByEmail(ArgumentMatchers.any(String.class))).thenReturn(Optional.of(user));

        boolean isUserExisting = userService.isUserExistingByEmail(email);

        assertThat(isUserExisting).isSameAs(true);
        verify(userRepository).findByEmail(email);
    }
    
}
