/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * @license
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Link } from "react-router-dom";

import './Registration.css';

import BaseUiComponent from '../base/BaseUiComponent';

/*
 * Component representing registration page.
 */
class RegistrationUiComponent extends BaseUiComponent {

    /*
     * Constructor setting state data and validation data to initial values.
     * 
     * @param {Object} props Generic properties to be passed into class.
     */
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            passwordRepeated: '',
            displayName: '',
            email: '',
            gender: ''
        };
        this.validation = {
            usernameNotMissing: true,
            passwordNotMissing: true,
            passwordRepeatedNotMissing: true,
            passwordsNotDiffer: true,
            displayNameNotMissing: true,
            emailNotMissing: true,
            genderNotMissing: true
        };
    };
    
    /*
     * Sets validation data.
     */
    setValidation = (validation) => {
        if ("usernameNotMissing" in validation) {
            this.validation.usernameNotMissing = validation.usernameNotMissing;
        }
        if ("passwordNotMissing" in validation) {
            this.validation.passwordNotMissing = validation.passwordNotMissing;
        }
        if ("passwordRepeatedNotMissing" in validation) {
            this.validation.passwordRepeatedNotMissing = validation.passwordRepeatedNotMissing;
        }
        if ("passwordsNotDiffer" in validation) {
            this.validation.passwordsNotDiffer = validation.passwordsNotDiffer;
        }
        if ("dispalyNameNotMissing" in validation) {
            this.validation.dispalyNameNotMissing = validation.dispalyNameNotMissing;
        }
        if ("emailNotMissing" in validation) {
            this.validation.emailNotMissing = validation.emailNotMissing;
        }
        if ("genderNotMissing" in validation) {
            this.validation.genderNotMissing = validation.genderNotMissing;
        }
    };
    
    /*
     * Checks that form data is valid.
     */
    isValid = () => {
        if (!this.validation.usernameNotMissing) {
            return false;
        }
        if (!this.validation.passwordNotMissing) {
            return false;
        }
        if (!this.validation.passwordRepeatedNotMissing) {
            return false;
        }
        if (!this.validation.passwordsNotDiffer) {
            return false;
        }
        if (!this.validation.displayNameNotMissing) {
            return false;
        }
        if (!this.validation.emailNotMissing) {
            return false;
        }
        if (!this.validation.genderNotMissing) {
            return false;
        }
        return true;
    };
    
    /*
     * Sets username in state.
     */
    setUsername = (username) => {
        this.setState({ username: username });
    };
    
    /*
     * Sets password in state.
     */
    setPassword = (password) => {
        this.setState({ password: password });
    };
    
    /*
     * Sets password (repeated) in state.
     */
    setPasswordRepeated = (passwordRepeated) => {
        this.setState({ passwordRepeated: passwordRepeated });
    };
    
    /*
     * Sets display name in state.
     */
    setDisplayName = (displayName) => {
        this.setState({ displayName: displayName });
    };
    
    /*
     * Sets email in state.
     */
    setEmail = (email) => {
        this.setState({ email: email });
    };
    
    /*
     * Sets gender in state.
     */
    setGender = (gender) => {
        this.setState({ gender: gender });
    };
    
    /*
     * Checks that no values are missing in form.
     */
    isAllSet = () => {
        if (this.state.username === '') {
            this.setValidation({ usernameNotMissing: false });
        } else {
            this.setValidation({ usernameNotMissing: true });
        }
        if (this.state.password === '') {
            this.setValidation({ passwordNotMissing: false });
        } else {
            this.setValidation({ passwordNotMissing: true });
        }
        if (this.state.passwordRepeated === '') {
            this.setValidation({ passwordRepeatedNotMissing: false });
        } else {
            this.setValidation({ passwordRepeatedNotMissing: true });
        }
        if (this.state.password !== this.state.passwordRepeated) {
            this.setValidation({ passwordsNotDiffer: false });
        } else {
            this.setValidation({ passwordsNotDiffer: true });
        }
        if (this.state.displayName === '') {
            this.setValidation({ displayNameNotMissing: false });
        } else {
            this.setValidation({ displayNameNotMissing: true });
        }
        if (this.state.email === '') {
            this.setValidation({ emailNotMissing: false });
        } else {
            this.setValidation({ emailNotMissing: true });
        }
        if (this.state.gender === '') {
            this.setValidation({ genderNotMissing: false });
        } else {
            this.setValidation({ genderNotMissing: true });
        }
        if (!this.isValid()) {
            return false;
        }
        return true;
    };
    
    /* 
     * Registers user via POST request to REST backend.
     */
    registerUser = async (userData) => {
        fetch('/restapi/auth/signup', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(userData)
        }).then(
            (response) => {
                if (response.ok && "json" in response) {
                    response.json().then(parsedJson => {
                        if ("registered" in parsedJson) {
                            if (parsedJson.registered === true) {
                                window.location.replace("/");
                            }                            
                        }
                    });
                }
            }
        );
    };
    
    /*
     * Submits form and creates user account with user data.
     */
    handleSubmit = async (e) => {
        e.preventDefault();
        if (this.isAllSet()) {
            await this.registerUser({
                username: this.state.username, 
                password: this.state.password,
                displayName: this.state.displayName,
                email: this.state.email,
                gender: this.state.gender
            });
        }
        
    };
    
    /*
     * Renders HTML of component taking data into account.
     */
    render() {
        return (
            <div>
                <h2>{this.t("registration")}</h2>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        <p>{this.t("username")}</p>
                        <input type="text" name="username" onChange={e => this.setUsername(e.target.value)} />
                    </label>
                    <label>
                        <p>{this.t("password")}</p>
                        <input type="password" name="password" onChange={e => this.setPassword(e.target.value)} />
                    </label>
                    <label>
                        <p>{this.t("password_repeated")}</p>
                        <input type="password" name="password_repeated" onChange={e => this.setPasswordRepeated(e.target.value)} />
                    </label>
                    <label>
                        <p>{this.t("display_name")}</p>
                        <input type="text" name="display_name" onChange={e => this.setDisplayName(e.target.value)} />
                    </label>
                    <label>
                        <p>{this.t("email")}</p>
                        <input type="text" name="email" onChange={e => this.setEmail(e.target.value)} />
                    </label>
                    <label>
                        <p>{this.t("gender")}</p>
                        <input type="radio" value="1" name="gender" onChange={e => this.setGender(e.target.value)} /> {this.t("diverse")}
                        <input type="radio" value="2" name="gender" onChange={e => this.setGender(e.target.value)} /> {this.t("female")}
                        <input type="radio" value="3" name="gender" onChange={e => this.setGender(e.target.value)} /> {this.t("male")}
                    </label>
                    <div>
                        <button type="submit">{this.t("register")}</button>
                    </div>
                    <div>
                        <Link to="/login">{this.t("back_login")}</Link>
                    </div>
                </form>
            </div>
        );
    }
};

export default RegistrationUiComponent;
