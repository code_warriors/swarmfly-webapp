/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * @license
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Outlet, Link } from "react-router-dom";
import { Button } from 'reactstrap';

import './Layout.css';

import BaseUiComponent from '../base/BaseUiComponent';

/*
 * Component representing layout page.
 */
class LayoutUiComponent extends BaseUiComponent {
    
    /*
     * Logs user out.
     */
    logout = () => {
        this.props.logout();
    };
    
    /*
     * Renders HTML of component taking data into account.
     */
    render() {    
        return (
            <>
                <nav>
                    <ul>
                        <li>
                            <Link to="/">{this.t("home")}</Link>
                        </li>
                        <li>
                            <Link to="/dashboard">{this.t("dashboard")}</Link>
                        </li>
                        <li>
                            <Link to="/preferences">{this.t("preferences")}</Link>
                        </li>
                    </ul>
                </nav>

                <Button onClick={() => this.logout()}>{this.t("logout")}</Button>

                <Outlet />
            </>
        );
    }
}

LayoutUiComponent.propTypes = {
    logout: PropTypes.func.isRequired
};

export default LayoutUiComponent;
