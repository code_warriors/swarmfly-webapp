/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * @license
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

import './Login.css';

import BaseUiComponent from '../base/BaseUiComponent';
import UnauthorizedUiComponent from '../http_status/UnauthorizedUiComponent';

/*
 * Component representing login page.
 */
class LoginUiComponent extends BaseUiComponent {
    
    /*
     * Constructor setting username, password and hasError in state to initial values.
     * 
     * @param {Object} props Generic properties to be passed into class.
     */
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            hasError: false
        };
    };
    
    /*
     * Sets username in state.
     */
    setUsername = (username) => {
        this.setState({ username: username });
    };
    
    /*
     * Sets password in state.
     */
    setPassword = (password) => {
        this.setState({ password: password });
    };
    
    /*
     * Sets hasError in state.
     */
    setError = (hasError) => {
        this.setState({ hasError: hasError });
    };
    
    /*
     * Sets JWT authentication token.
     */
    setToken = (token) => {
        this.props.setToken(token);
    };
    
    /*
     * Logs user out.
     */
    logout = () => {
        this.props.logout();
    };
    
    /* 
     * Logs user in via POST request to REST backend and store JWT for authentication.
     */
    loginUser = async (credentials) => {
        fetch('/restapi/auth/signin', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }).then(
            (response) => {
                if (response.ok && "json" in response) {
                    response.json().then(parsedJson => {
                        if ("token" in parsedJson) {
                            this.setToken(parsedJson.token);
                            this.setError(false);
                            return;
                        }
                    });
                }
                this.setError(true);
            }
        );
    };
    
    /*
     * Submits form and logs user in with username-password credentials.
     */
    handleSubmit = async (e) => {
        e.preventDefault();
        await this.loginUser({
            username: this.state.username, 
            password: this.state.password
        });
    };

    /*
     * Renders HTML of component taking data into account.
     */
    render() {
        if (this.state.hasError) {
            return (
                <div>
                    <UnauthorizedUiComponent t={this.t} />
                </div>
            );
        }
        
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    <p>{this.t("username")}</p>
                    <input type="text" name="username" 
                        onChange={e => this.setUsername(e.target.value)} />
                </label>
                <label>
                    <p>{this.t("password")}</p>
                    <input type="password" name="password" onChange={e => this.setPassword(e.target.value)} />
                </label>
                <div>
                    <button type="submit">{this.t("login")}</button>
                </div>
                <div>
                    <Link to="/register" onClick={ e => e }>{this.t("go_register")}</Link>
                </div>
            </form>
        );
    }
}

LoginUiComponent.propTypes = {
    setToken: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired
};

export default LoginUiComponent;
