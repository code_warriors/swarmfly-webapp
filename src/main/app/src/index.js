/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * @license
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import './index.css';

import App from './App';

/*
 * Configures translations into different languages.
 */
i18n
    .use(initReactI18next)
    .init({
        resources: {
            en: {
                translation: {
                    "application": "Application",
                    "back_login": "Go back to login page.",
                    "dashboard": "Dashboard",
                    "display_name": "Display name",
                    "diverse": "Diverse",
                    "email": "Email",
                    "female": "Female",
                    "gender": "Gender",
                    "go_register": "Go and register yourself, now!",
                    "home": "Home",
                    "login": "Login",
                    "logout": "Logout",
                    "male": "Male",
                    "page_not_found": "Page not found",
                    "password": "Password",
                    "password_repeated": "Password repeated",
                    "preferences": "Preferences",
                    "register": "Register",
                    "registration": "Registration",
                    "unauthorized": "Unauthorized",
                    "username": "Username"
                }
            },
            de: {
                translation: {
                    "application": "Anwendung",
                    "back_login": "Zurück zur Loginseite",
                    "dashboard": "Dashboard",
                    "display_name": "Anzeigename",
                    "diverse": "Divers",
                    "email": "Email",
                    "female": "Weiblich",
                    "gender": "Geschlecht",
                    "go_register": "Registriere dich, jetzt!",
                    "home": "Home",
                    "login": "Login",
                    "logout": "Logout",
                    "male": "Männlich",
                    "page_not_found": "Seite nicht gefunden",
                    "password": "Passwort",
                    "password_repeated": "Passwort wiederholt",
                    "preferences": "Einstellungen",
                    "register": "Registrieren",
                    "registration": "Registrierung",
                    "unauthorized": "Unauthorisiert",
                    "username": "Benutzername"
                }
            }
        },
        lng: "de",
        fallbackLng: "en",

        interpolation: {
            escapeValue: false
        }
    });

/*
 * Renders app and hooks it into tag with root ID.
 */
ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById('root')
);
