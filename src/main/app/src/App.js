/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * @license
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import './App.css';

import DashboardUiComponent from './dashboard/DashboardUiComponent';
import HomeUiComponent from './home/HomeUiComponent';
import LayoutUiComponent from './layout/LayoutUiComponent';
import LoginUiComponent from './login/LoginUiComponent';
import NotFoundUiComponent from './http_status/NotFoundUiComponent';
import PreferencesUiComponent from './preferences/PreferencesUiComponent';
import RegistrationUiComponent from './registration/RegistrationUiComponent';

/*
 * Component that provides the frontend app managing the state containing the JWT authenticaiton token.
 */
class AppComponent extends React.Component {
    
    /*
     * Constructor setting token in state to empty string.
     * 
     * @param {Object} props Generic properties to be passed into class.
     */
    constructor(props) {
        super(props);
        this.state = {
            token: ''
        };
    };
    
    /*
     * Translating key in different languages.
     * 
     * @param {string} key Key to be translated.
     * @returns {string} Translated text.
     */
    t = (key) => {
        return this.props.t(key);
    }
    
    /*
     * Called if component is mounted and the stored JWT authentication token can be retrieved.
     */
    componentWillMount() {
        const token = sessionStorage.getItem('token');
        if (token && !this.state.token) {
            this.setState({ token: token });
        }
    };
    
    /*
     * Stores JWT authentication token.
     */
    setToken = (token) => {
        sessionStorage.setItem('token', token);
        this.setState({ token: token });
    };
    
    /*
     * Clears JWT authentication token.
     */
    logout = () => {
        this.setState({ token: '' });
        sessionStorage.clear();
    };
    
    /*
     * Renders HTML of component taking data into account.
     */
    render() {
        if (this.state.token === "") {
            return (
                <div>
                    <BrowserRouter>
                        <Routes>
                            <Route path="/login" element={<LoginUiComponent t={this.t} setToken={this.setToken} logout={this.logout} />} />
                            <Route path="/register" element={<RegistrationUiComponent t={this.t} />} />
                            <Route path="/*" element={<Navigate replace to="/login" />} />
                        </Routes>
                    </BrowserRouter>
                </div>
            );
        }
        
        return (
            <div>
                <h1>{this.t("application")}</h1>
                <BrowserRouter>
                    <Routes>
                        <Route path="/" element={<LayoutUiComponent t={this.t} logout={this.logout} />}>
                            <Route index element={<HomeUiComponent t={this.t} />} />
                            <Route path="dashboard" element={<DashboardUiComponent t={this.t} />} />
                            <Route path="preferences" element={<PreferencesUiComponent t={this.t} />} />
                            <Route path="404" element={<NotFoundUiComponent t={this.t} />} />
                            <Route path="login" element={<Navigate replace to="/" />} />
                            <Route path="*" element={<Navigate replace to="404" />} />
                        </Route>
                    </Routes>
                </BrowserRouter>
            </div>
        );
    }
}
 
function App() {
    const { t } = useTranslation();
    return (
        <AppComponent t={t} />
    );
};

export default App;
    