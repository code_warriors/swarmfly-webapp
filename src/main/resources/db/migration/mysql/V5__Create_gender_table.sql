# SPDX-FileCopyrightText: 2022 Code Warriors
#
# SPDX-License-Identifier: AGPL-3.0-only

# Swarmfly Web - A Group Activity Coordination Management Webapp
#
# Copyright (C) 2022 Code Warriors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CREATE TABLE IF NOT EXISTS `genders` (
  `gender_id` int NOT NULL AUTO_INCREMENT,
  `gender_name` varchar(255) NOT NULL,
  PRIMARY KEY (`gender_id`),
  UNIQUE KEY `gender_id_UNIQUE` (`gender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT IGNORE INTO `swarmfly_db`.`genders` (`gender_name`) VALUES ("diverse");
INSERT IGNORE INTO `swarmfly_db`.`genders` (`gender_name`) VALUES ("female");
INSERT IGNORE INTO `swarmfly_db`.`genders` (`gender_name`) VALUES ("male");

ALTER TABLE `swarmfly_db`.`users` ADD `gender_id` int NOT NULL DEFAULT 0;
