package com.gitlab.code_warriors.swarmfly_webapp.registration_component.model;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import java.io.Serializable;

/**
 * Model class for registration response data.
 * 
 * @author christian.hueser
 */
public class RegistrationResponse implements Serializable {

    /**
     * UID for serialization.
     */
    private static final long serialVersionUID = -2338626292552177485L;
    
    /**
     * Flag to indicate that user has been registered to be included in the response object.
     */
    private final boolean isRegistered;

    /**
     * Constructor to set flag to indicate that user has been registered.
     * 
     * @param isRegistered boolean Flag to indicate that user has been registered.
     */
    public RegistrationResponse(boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

    /**
     * Returns flag to indicate that user has been registered to be included in the response object.
     * 
     * @return String Flag to indicate that user has been registered.
     */
    public boolean isRegistered() {
        return this.isRegistered;
    }
    
}
