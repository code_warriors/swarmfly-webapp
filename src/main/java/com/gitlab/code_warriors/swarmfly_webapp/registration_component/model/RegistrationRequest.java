package com.gitlab.code_warriors.swarmfly_webapp.registration_component.model;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import java.io.Serializable;

/**
 * Model class for registration request data.
 * 
 * @author christian.hueser
 */
public class RegistrationRequest implements Serializable {

    /**
     * UID for serialization.
     */
    private static final long serialVersionUID = 2338626292552177485L;

    /**
     * Username of a user.
     */
    private String username;
    
    /**
     * Password of a user.
     */
    private String password;

    /**
     * Display name of a user.
     */
    private String displayName;
    
    /**
     * Email of a user.
     */
    private String email;
    
    /**
     * Gender of a user.
     */
    private String gender;
    
    /**
     * Default for JSON Parsing.
     */
    public RegistrationRequest()
    {
    }

    /**
     * Constructor setting username, password, display name, email, gender of a user.
     * 
     * @param username String Username of a user.
     * @param password String Password of a user.
     * @param email String Email of a user.
     * @param gender String Gender of a user.
     */
    public RegistrationRequest(String username, String password, String displayName, String email, String gender) {
        this.username = username;
        this.password = password;
        this.displayName = displayName;
        this.email = email;
        this.gender = gender;
    }

    /**
     * Returns username of a users.
     * 
     * @return String Username of a user.
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Sets username of a user.
     * 
     * @param username String Username of a user.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Returns password of a user.
     * 
     * @return String Password of a user.
     */
    public String getPassword() {
        return this.password;
    }
    
    /**
     * Sets password of a user.
     * 
     * @param password Password of a user.
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * Returns display name of a user.
     * 
     * @return String Display name of a user.
     */
    public String getDisplayName() {
        return this.displayName;
    }
    
    /**
     * Sets display name of a user.
     * 
     * @param displayName Display names of a user.
     */
    public void setDisplayNamed(String displayName) {
        this.displayName = displayName;
    }
    
    /**
     * Returns email of a user.
     * 
     * @return String Email of a user.
     */
    public String getEmail() {
        return this.email;
    }
    
    /**
     * Sets email of a user.
     * 
     * @param email Email of a user.
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    /**
     * Returns gender of a user.
     * 
     * @return String Gender of a user.
     */
    public String getGender() {
        return this.gender;
    }
    
    /**
     * Sets gender of a user.
     * 
     * @param gender Gender of a user.
     */
    public void setGender(String gender) {
        this.gender = gender;
    }
    
}
