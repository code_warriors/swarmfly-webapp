package com.gitlab.code_warriors.swarmfly_webapp.registration_component.controller;

import com.gitlab.code_warriors.swarmfly_webapp.credential_component.entity.CredentialEntity;
import com.gitlab.code_warriors.swarmfly_webapp.credential_component.service.ICredentialService;
import com.gitlab.code_warriors.swarmfly_webapp.registration_component.model.RegistrationRequest;
import com.gitlab.code_warriors.swarmfly_webapp.registration_component.model.RegistrationResponse;
import com.gitlab.code_warriors.swarmfly_webapp.user_component.entity.GenderEntity;
import com.gitlab.code_warriors.swarmfly_webapp.user_component.entity.UserEntity;
import com.gitlab.code_warriors.swarmfly_webapp.user_component.service.IGenderService;
import com.gitlab.code_warriors.swarmfly_webapp.user_component.service.IUserService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Controller class to define routes to registration functionality.
 * 
 * @author christian.hueser
 */
@RestController
@RequestMapping("/restapi")
public class RegistrationController {

    /**
     * User service auto-wired into controller.
     */
    @Autowired
    private IUserService userService;
    
    /**
     * Gender service auto-wired into controller.
     */
    @Autowired
    private IGenderService genderService;
    
    /**
     * User service auto-wired into controller.
     */
    @Autowired
    private ICredentialService credentialService;
    
    /**
     * Controller action to generate authentication token on POST request to sign in.
     * 
     * @param registrationRequest RegistrationRequest Registration request used for user registration.
     * @return ResponseEntity Response object with HTTP status and registration status to be sent.
     * @throws Exception Thrown if registration failed.
     */
    @RequestMapping(value = "/auth/signup", method = RequestMethod.POST)
    public ResponseEntity<?> saveRegistration(@RequestBody RegistrationRequest registrationRequest) throws Exception {
        
        Optional<UserEntity> userByUsername = userService.getUserByUsername(registrationRequest.getUsername());
        Optional<UserEntity> userByEmail = userService.getUserByEmail(registrationRequest.getEmail());
        
        if (userByUsername.isPresent() || userByEmail.isPresent()) {
            return new ResponseEntity<RegistrationResponse>(new RegistrationResponse(false), HttpStatus.CONFLICT);
        }
        
        UserEntity user = new UserEntity();
        user.setUsername(registrationRequest.getUsername());
        user.setDisplayName(registrationRequest.getDisplayName());
        user.setEmail(registrationRequest.getEmail());
        Optional<GenderEntity> gender = genderService.getGenderByGenderId(
                Long.parseLong(registrationRequest.getGender()));
        user.setGender(gender.get());
        
        UserEntity userEntity = userService.saveUser(user);
        
        CredentialEntity credential = new CredentialEntity();
        String encryptedPassword = new BCryptPasswordEncoder().encode(registrationRequest.getPassword());
        credential.setPassword(encryptedPassword);
        credential.setUser(userEntity);
        CredentialEntity credentialEntity = credentialService.saveCredential(credential);
        
        return new ResponseEntity<RegistrationResponse>(new RegistrationResponse(true), HttpStatus.CREATED);
        
    }
}
