package com.gitlab.code_warriors.swarmfly_webapp.user_component.repository;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.gitlab.code_warriors.swarmfly_webapp.user_component.entity.UserEntity;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository interface for CRUD operations on users.
 * 
 * @author christian.hueser
 */
@Repository
public interface IUserRepository extends CrudRepository<UserEntity, Long> {
    
    /**
     * Finds a user entity by username given.
     * 
     * @param username String Username of a user.
     * @return UserEntity User entity that corresponds to the username given.
     */
    Optional<UserEntity> findByUsername(String username);
    
    /**
     * Finds a user entity by email given.
     * 
     * @param email String Email of a user.
     * @return UserEntity User entity that corresponds to the email given.
     */
    Optional<UserEntity> findByEmail(String email);
    
    /**
     * Determines whether a user given the username exists.
     * 
     * @param username String Username of a user.
     * @return Boolean True if user with username given exists, false otherwise.
     */
    Boolean existsByUsername(String username);
    
    /**
     * Determines whether a user given the email exists.
     * 
     * @param email String Email of a user.
     * @return Boolean True if user with email given exists, false otherwise.
     */
    Boolean existsByEmail(String email);
    
    /**
     * Saves user entity.
     * 
     * @param user UserEntity User entity to be saved.
     * @return UserEntity Saved user entity.
     */
    @Override
    UserEntity save(UserEntity user);
    
}
