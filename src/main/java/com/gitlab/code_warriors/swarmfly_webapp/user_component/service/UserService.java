package com.gitlab.code_warriors.swarmfly_webapp.user_component.service;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.gitlab.code_warriors.swarmfly_webapp.user_component.entity.UserEntity;
import com.gitlab.code_warriors.swarmfly_webapp.user_component.repository.IUserRepository;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class to load users.
 * 
 * @author christian.hueser
 */
@Service
@Transactional
public class UserService implements IUserService {
	
    /**
     * User repository auto-wired into service.
     */
    @Autowired
    private IUserRepository userRepository;

    /**
     * Finds all user entities that exist.
     * 
     * @return Iterable List of user entities.
     */
    @Override
    public Iterable<UserEntity> getAllUsers() {
        Iterable<UserEntity> list = userRepository.findAll();
        return list;
    }

    /**
     * Finds a user entity by the user ID given.
     * 
     * @param userId Long ID of the user entity.
     * @return UserEntity User entity of a user.
     */
    @Override
    public Optional<UserEntity> getUserByUserId(Long userId) {
        Optional<UserEntity> user = userRepository.findById(userId);
        return user;
    }
    
    /**
     * Finds a user entity by the username given.
     * 
     * @param username String Username of the user.
     * @return UserEntity User entity of a user.
     */
    @Override
    public Optional<UserEntity> getUserByUsername(String username) {
        Optional<UserEntity> user = userRepository.findByUsername(username);
        return user;
    }
    
    /**
     * Finds a user entity by the email given.
     * 
     * @param email String Email of the user.
     * @return UserEntity User entity of a user.
     */
    @Override
    public Optional<UserEntity> getUserByEmail(String email) {
        Optional<UserEntity> user = userRepository.findByEmail(email);
        return user;
    }
    
    /**
     * Determines whether user entity exists given the username of the user.
     * 
     * @param username string Username of a user.
     * @return Boolean True if user given the username exists, false otherwise.
     */
    @Override
    public Boolean isUserExistingByUsername(String username) {
        Optional<UserEntity> user = userRepository.findByUsername(username);
        return user.isPresent();
    }
    
    /**
     * Determines whether user entity exists given the email of the user.
     * 
     * @param email string Email of a user.
     * @return Boolean True if user given the email exists, false otherwise.
     */
    @Override
    public Boolean isUserExistingByEmail(String email) {
        Optional<UserEntity> user = userRepository.findByEmail(email);
        return user.isPresent();
    }
    
    /**
     * Saves user entity.
     * 
     * @param user UserEntity User entity to be saved.
     * @return UserEntity Saved user entity.
     */
    @Override
    public UserEntity saveUser(UserEntity user) {
        
        UserEntity userEntity = userRepository.save(user);
        return userEntity;
        
    }
    
}
