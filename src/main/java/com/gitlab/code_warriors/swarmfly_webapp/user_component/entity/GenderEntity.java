package com.gitlab.code_warriors.swarmfly_webapp.user_component.entity;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class to model genders.
 * 
 * @author christian.hueser
 */
@Entity
@Table(name = "genders")
public class GenderEntity implements Serializable {
    
    /**
     * Id of a gender entity.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "gender_id")
    private Long id;
    
    /**
     * Name of the respective gender.
     */
    @Column(name = "gender_name")
    private String genderName;
    
    /**
     * Returns an ID of a gender entity.
     * 
     * @return Long ID of a gender entity.
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the ID of a gender entity.
     * 
     * @param id Long ID of a gender entity.
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * Returns the name of a respective gender.
     * 
     * @return String Name of a respective gender.
     */
    public String getGenderName() {
        return this.genderName;
    }

    /**
     * Sets the name of a respective gender.
     * 
     * @param genderName String Name of a respective gender.
     */
    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }
    
}
