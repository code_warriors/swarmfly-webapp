package com.gitlab.code_warriors.swarmfly_webapp.user_component.entity;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Entity class to model users.
 * 
 * @author christian.hueser
 */
@Entity
@Table(name = "users")
public class UserEntity implements Serializable {

    /**
     * Id of a user entity.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;

    /**
     * Username of the user.
     */
    @Column(name = "username")
    private String username;

    /**
     * Email of the user.
     */
    @Column(name = "email")
    private String email;

    /**
     * Display name of the user.
     */
    @Column(name = "displayname")
    private String displayName;
    
    /**
     * Relationship to gender with foreign key.
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "gender_id")
    private GenderEntity gender;
    
    /**
     * Returns Id of a user entity.
     * 
     * @return Long ID of a user entity.
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets ID of a user entity.
     * 
     * @param id Long ID of a user entity.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns username of a user.
     * 
     * @return String Username of a user.
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Sets username of a user.
     * 
     * @param username String Username of a user.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Returns email of a user.
     * 
     * @return String Email of a user.
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Sets email of a user.
     * 
     * @param email String Email of a user.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Returns display name of a user.
     * 
     * @return String Display name of a user.
     */
    public String getDisplayName() {
        return this.displayName;
    }

    /**
     * Sets display name of a user.
     * 
     * @param displayName String Display name of a user.
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    
    /**
     * Returns a gender entity related to a user entity.
     * 
     * @return GenderEntity Gender entity related to a user entity.
     */
    public GenderEntity getGender() {
        return this.gender;
    }

    /**
     * Sets the gender entity related to a user entity.
     * 
     * @param gender Gender entity related to a user entity.
     */
    public void setGender(GenderEntity gender) {
        this.gender = gender;
    }

}
