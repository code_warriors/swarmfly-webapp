package com.gitlab.code_warriors.swarmfly_webapp.user_component.service;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.gitlab.code_warriors.swarmfly_webapp.user_component.entity.GenderEntity;
import com.gitlab.code_warriors.swarmfly_webapp.user_component.repository.IGenderRepository;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class to load genders.
 * 
 * @author christian.hueser
 */
@Service
@Transactional
public class GenderService implements IGenderService {
	
    /**
     * Gender repository auto-wired into service.
     */
    @Autowired
    private IGenderRepository genderRepository;

    /**
     * Finds all gender entities that exist.
     * 
     * @return Iterable List of gender entities.
     */
    @Override
    public Iterable<GenderEntity> getAllGenders() {
        Iterable<GenderEntity> list = genderRepository.findAll();
        return list;
    }

    /**
     * Finds a gender entity by the user ID given.
     * 
     * @param genderId Long ID of the user entity.
     * @return GenderEntity Gender entity of a user.
     */
    @Override
    public Optional<GenderEntity> getGenderByGenderId(Long genderId) {
        Optional<GenderEntity> gender = genderRepository.findById(genderId);
        return gender;
    }
    
}
