package com.gitlab.code_warriors.swarmfly_webapp.user_component.controller;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.gitlab.code_warriors.swarmfly_webapp.user_component.entity.UserEntity;
import com.gitlab.code_warriors.swarmfly_webapp.user_component.service.IUserService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class to define routes to user functionality.
 * 
 * @author christian.hueser
 */
@RestController
@RequestMapping("/restapi")
public class UserController {

    /**
     * User service auto-wired into controller.
     */
    @Autowired
    private IUserService userService;

    /**
     * Controller action to return user entity given the user ID from HTTP GET request.
     * 
     * @param id String ID of the user entity.
     * @return ResponseEntity Response containing user entity that corresponds to the user ID.
     */
    @GetMapping(path = "/user/id/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Optional<UserEntity>> index(@PathVariable String id) {

        Optional<UserEntity> user = userService.getUserByUserId(Long.parseLong(id));

        return new ResponseEntity<Optional<UserEntity>>(user, HttpStatus.OK);
    }

}
