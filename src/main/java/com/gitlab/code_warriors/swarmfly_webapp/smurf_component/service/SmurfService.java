package com.gitlab.code_warriors.swarmfly_webapp.smurf_component.service;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.gitlab.code_warriors.swarmfly_webapp.smurf_component.entity.SmurfEntity;
import com.gitlab.code_warriors.swarmfly_webapp.smurf_component.repository.ISmurfRepository;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class to load smurfs.
 * 
 * @author christian.hueser
 */
@Service
@Transactional
public class SmurfService implements ISmurfService {
	
    /**
     * Smurf repository auto-wired into service.
     */
    @Autowired
    private ISmurfRepository smurfRepository;		   

    /**
     * Get all smurfs that exist.
     * 
     * @return Iterable List of smurf entities.
     */
    @Override
    public Iterable<SmurfEntity> getAllSmurfs() {
        Iterable<SmurfEntity> list = smurfRepository.findAll();
        return list;
    }
    
    /**
     * Get a smurf entity by smurf ID given.
     * 
     * @param smurfId Long Id of the smurf entity.
     * @return SmurfEntity Smurf entity corresponding to smurf ID given.
     */
    @Override
    public Optional<SmurfEntity> getSmurfBySmurfId(Long smurfId) {
        Optional<SmurfEntity> smurf = smurfRepository.findById(smurfId);
        return smurf;
    }
      
}
