package com.gitlab.code_warriors.swarmfly_webapp.smurf_component.entity;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class to model smurfs.
 * 
 * @author christian.hueser
 */
@Entity
@Table(name = "smurf")
public class SmurfEntity implements Serializable {
    
    /**
     * ID of a smurf entity.
     */
    @Id
    private Long id;
    
    /**
     * Name of the smurf.
     */
    @Column(name = "name")
    private String name;

    /**
     * Type of the smurf.
     */
    @Column(name = "type")
    private String type;
    
    /**
     * Returns ID of the smurf entity.
     * 
     * @return Long ID of the smurf entity.
     */
    public Long getId() {
        return id;
    }
    
    /**
     * Sets the ID of the smurf entity.
     * 
     * @param id Long ID of the smurf entity.
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * Returns the name of the smurf.
     * 
     * @return String Name of the smurf.
     */
    public String getName() {
        return name;
    }
    
    /**
     * Sets the name of the smurf.
     * 
     * @param name String Name of the smurf.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Returns the type of the smurf.
     * 
     * @return String Type of the smurf.
     */
    public String getType() {
        return type;
    }
    
    /**
     * Sets the type of the smurf.
     * 
     * @param type String Type of the smurf.
     */
    public void setType(String type) {
        this.type = type;
    }
    
}