package com.gitlab.code_warriors.swarmfly_webapp.smurf_component.service;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.gitlab.code_warriors.swarmfly_webapp.smurf_component.entity.SmurfEntity;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 * Service interface to load smurfs.
 * 
 * @author christian.hueser
 */
@Service
@Transactional
public interface ISmurfService {

    /**
     * Get all smurfs that exist.
     * 
     * @return Iterable List of smurf entities.
     */
    public Iterable<SmurfEntity> getAllSmurfs();

    /**
     * Get a smurf entity by smurf ID given.
     * 
     * @param smurfId Long Id of the smurf entity.
     * @return SmurfEntity Smurf entity corresponding to smurf ID given.
     */
    public Optional<SmurfEntity> getSmurfBySmurfId(Long smurfId);

}

