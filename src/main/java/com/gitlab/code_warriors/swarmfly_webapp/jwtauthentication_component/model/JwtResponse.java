package com.gitlab.code_warriors.swarmfly_webapp.jwtauthentication_component.model;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import java.io.Serializable;

/**
 * Model class for authentication response data.
 * 
 * Implementation is based on an article on JWT Authentication with Spring Boot
 * by TechGeekNext:
 * https://www.techgeeknext.com/spring/spring-boot-security-token-authentication-jwt
 * 
 * @author christian.hueser
 */
public class JwtResponse implements Serializable {

    /**
     * UID for serialization.
     */
    private static final long serialVersionUID = -8091879091924046844L;
    
    /**
     * JWT token of the current user session to be included in the response object.
     */
    private final String jwttoken;

    /**
     * Sets JWT token in response object.
     * 
     * @param jwttoken String JWT token of the current user session to be included in the response object.
     */
    public JwtResponse(String jwttoken) {
        this.jwttoken = jwttoken;
    }

    /**
     * Returns JWT token of the current user session to be included in the response object.
     * 
     * @return String JWT token of the current user session to be included in the response object.
     */
    public String getToken() {
        return this.jwttoken;
    }

}
