package com.gitlab.code_warriors.swarmfly_webapp.jwtauthentication_component.controller;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.gitlab.code_warriors.swarmfly_webapp.jwtauthentication_component.config.JwtTokenUtil;
import com.gitlab.code_warriors.swarmfly_webapp.jwtauthentication_component.model.JwtRequest;
import com.gitlab.code_warriors.swarmfly_webapp.jwtauthentication_component.model.JwtResponse;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class to generate and send JWTs and authenticate requests.
 * 
 * Implementation is based on an article on JWT Authentication with Spring Boot
 * by TechGeekNext:
 * https://www.techgeeknext.com/spring/spring-boot-security-token-authentication-jwt
 * 
 * @author christian.hueser
 */
@RestController
@RequestMapping("/restapi")
public class JwtAuthenticationController {

    /**
     * Authentication manager auto-wired into controller.
     */
    @Autowired
    private AuthenticationManager authenticationManager;

    /**
     * User details service auto-wired into controller.
     */
    @Autowired
    private UserDetailsService jwtUserDetailsService;

    /**
     * JWT token utilities auto-wired into controller.
     */
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    
    /**
     * Controller action to generate authentication token on POST request to sign in.
     * 
     * @param authenticationRequest JwtRequest JWT request used for user authentication.
     * @return ResponseEntity Response object with HTTP status and JWT token to be sent.
     * @throws Exception Thrown if generation of authentication token failed.
     */
    @RequestMapping(value = "/auth/signin", method = RequestMethod.POST)
    public ResponseEntity<?> generateAuthenticationToken(@RequestBody JwtRequest authenticationRequest)
            throws Exception {
        
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        
        final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponse(token));
    }

    /**
     * Authenticate user by given username and password credentials.
     * 
     * @param username String Username of the user who is authenticating.
     * @param password String Password of the user who is authenticating.
     * @throws Exception Thrown if user is disabled or credentials are invalid.
     */
    private void authenticate(String username, String password) throws Exception {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

}
