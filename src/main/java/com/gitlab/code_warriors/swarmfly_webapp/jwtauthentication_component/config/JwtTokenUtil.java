package com.gitlab.code_warriors.swarmfly_webapp.jwtauthentication_component.config;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

/**
 * Class to generate and validate JWTs based on secrets.
 * 
 * Implementation is based on an article on JWT Authentication with Spring Boot
 * by TechGeekNext:
 * https://www.techgeeknext.com/spring/spring-boot-security-token-authentication-jwt
 * 
 * @author christian.hueser
 */
@Component
public class JwtTokenUtil implements Serializable {

    /**
     * UID for serialization.
     */
	private static final long serialVersionUID = -2550185165626007488L;

    /**
     * Validity duration of JWT token.
     */
	public static final long JWT_TOKEN_VALIDITY = 5*60*60;

    /**
     * Secret JWT signing key for securely generating JWT tokens.
     */
	@Value("${swarmfly.app.jwtSecret}")
	private String secret;

    /**
     * Returns username as claim subject from JWT token.
     * 
     * @param token String Current JWT token.
     * @return String Username as a claim subject.
     */
	public String getUsernameFromToken(String token) {
		return getClaimFromToken(token, Claims::getSubject);
	}

    /**
     * Returns issued at date-time as issued at claim from JWT token.
     * 
     * @param token String Current JWT token.
     * @return Date Issued at date-time as issued at claim.
     */
	public Date getIssuedAtDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getIssuedAt);
	}

    /**
     * Returns expiration date-time as expiration claim from JWT token.
     * 
     * @param token String Current JWT token.
     * @return Date Expiration date-time as expiration claim.
     */
	public Date getExpirationDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getExpiration);
	}
    
    /**
     * Resolves claims from given JWT token and claim resolver function.
     * 
     * @param token String Current JWT token.
     * @param claimsResolver Claim resolver function that resolves claims to a particular claim.
     * @return T Resolved claim that belongs to JWT token by given claim resolver function.
     */
	public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token);
		return claimsResolver.apply(claims);
	}

    /**
     * Returns all claims of a given JWT token by utilizing JWT signing key.
     * 
     * @param token String Current JWT token.
     * @return Claims All claims of a given JWT token and JWT signing key.
     */
	private Claims getAllClaimsFromToken(String token) {
		return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
	}

    /**
     * Determines whetherJWT token is expired.
     * 
     * @param token String Current JWT token.
     * @return Boolean True if JWT token is expired, false otherwise.
     */
	private Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}

    /**
     * Determines whether JWT token expiration should be ignored.
     * 
     * @param token String Current JWT token.
     * @return Boolean True if token expiration should be ignored, false otherwise.
     */
	private Boolean ignoreTokenExpiration(String token) {
		// here you specify tokens, for that the expiration is ignored
		return false;
	}

    /**
     * Generates a JWT token by given user details object.
     * 
     * @param userDetails UserDetails User details object containing username.
     * @return String Generated JWT token.
     */
	public String generateToken(UserDetails userDetails) {
		Map<String, Object> claims = new HashMap<>();
		return doGenerateToken(claims, userDetails.getUsername());
	}
    
    /**
     * Generates a JWT token by providing a claim subject and signing it with a signature algorithm and signing key.
     * 
     * @param claims Map<String, Object> Claims that contain subject, issued at and expiration information.
     * @param subject String Username as claim subject.
     * @return String Generated JWT token.
     */
	private String doGenerateToken(Map<String, Object> claims, String subject) {
		return Jwts.builder().setClaims(claims)
                             .setSubject(subject)
                             .setIssuedAt(new Date(System.currentTimeMillis()))
                             .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY*1000))
                             .signWith(SignatureAlgorithm.HS512, secret)
                             .compact();
	}

    /**
     * Determines whether JWT token can be refreshed depending on token expiration and if token should not be ignored.
     * 
     * @param token String Current JWT token.
     * @return Boolean True if JWT token can be refreshed, false otherwise.
     */
	public Boolean canTokenBeRefreshed(String token) {
		return (!isTokenExpired(token) || ignoreTokenExpiration(token));
	}

    /**
     * Determines whether given JWT token is valid for given username.
     * 
     * @param token String Current JWT token.
     * @param userDetails User details that contain username.
     * @return Boolean True if username equals username in JWT token and if JWT token is not expired, false otherwise.
     */
	public Boolean validateToken(String token, UserDetails userDetails) {
		final String username = getUsernameFromToken(token);
		return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
	}
}
