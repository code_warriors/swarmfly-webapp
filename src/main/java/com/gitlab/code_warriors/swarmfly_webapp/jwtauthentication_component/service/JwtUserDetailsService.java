package com.gitlab.code_warriors.swarmfly_webapp.jwtauthentication_component.service;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.gitlab.code_warriors.swarmfly_webapp.credential_component.entity.CredentialEntity;
import com.gitlab.code_warriors.swarmfly_webapp.credential_component.repository.ICredentialRepository;
import com.gitlab.code_warriors.swarmfly_webapp.user_component.entity.UserEntity;
import com.gitlab.code_warriors.swarmfly_webapp.user_component.repository.IUserRepository;
import java.util.ArrayList;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Service class to load user by its username.
 * 
 * Implementation is based on an article on JWT Authentication with Spring Boot
 * by TechGeekNext:
 * https://www.techgeeknext.com/spring/spring-boot-security-token-authentication-jwt
 * 
 * @author christian.hueser
 */
@Service
@Transactional
public class JwtUserDetailsService implements UserDetailsService {

    /**
     * Credential repository auto-wired into service.
     */
    @Autowired
    private ICredentialRepository credentialRepository;

    /**
     * User repository auto-wired into service.
     */
    @Autowired
    private IUserRepository userRepository;

    /**
     * Loads user details from username given.
     * 
     * @param username String Username of the user.
     * @return UserDetails User details corresponding to the username given.
     * @throws UsernameNotFoundException Thrown if username given was not found.
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<UserEntity> user = userRepository.findByUsername(username);
        if (user.isPresent()) {
            Optional<CredentialEntity> credential = credentialRepository.findByUserId(user.get().getId());
            if (credential.isPresent()) {
                return new User(credential.get().getUser().getUsername(), 
                                credential.get().getPassword(),
                                new ArrayList<>());
            }

        }
        throw new UsernameNotFoundException("User not found with username: " + username);
    }

}
