package com.gitlab.code_warriors.swarmfly_webapp.jwtauthentication_component.model;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import java.io.Serializable;

/**
 * Model class for authentication request data.
 * 
 * Implementation is based on an article on JWT Authentication with Spring Boot
 * by TechGeekNext:
 * https://www.techgeeknext.com/spring/spring-boot-security-token-authentication-jwt
 * 
 * @author christian.hueser
 */
public class JwtRequest implements Serializable {

    /**
     * UID for serialization.
     */
    private static final long serialVersionUID = 5926468583005150707L;

    /**
     * Username of a user.
     */
    private String username;
    
    /**
     * Password of a user.
     */
    private String password;

    /**
     * Default for JSON Parsing.
     */
    public JwtRequest()
    {
    }

    /**
     * Constructor setting username and password of a user.
     * 
     * @param username String Username of a user.
     * @param password String Password of a user.
     */
    public JwtRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * Returns username of a users.
     * 
     * @return String Username of a user.
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Sets username of a user.
     * 
     * @param username String Username of a user.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Returns password of a user.
     * 
     * @return String Password of a user.
     */
    public String getPassword() {
        return this.password;
    }
    
    /**
     * Sets password of a user.
     * 
     * @param password Password of a user.
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
