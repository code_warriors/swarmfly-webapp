package com.gitlab.code_warriors.swarmfly_webapp.jwtauthentication_component.config;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.gitlab.code_warriors.swarmfly_webapp.jwtauthentication_component.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Class to configure password encoder and routes that need authentication.
 * 
 * Implementation is based on an article on JWT Authentication with Spring Boot
 * by TechGeekNext:
 * https://www.techgeeknext.com/spring/spring-boot-security-token-authentication-jwt
 * 
 * @author christian.hueser
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * Auto-wired authentication entry point to send unauthorized error response.
     */
	@Autowired
	private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    /**
     * User details service auto-wired into web security configurations.
     */
	@Autowired
	private JwtUserDetailsService jwtUserDetailsService;

    /**
     * JWT request filter auto-wired into web security configurations.
     */
	@Autowired
	private JwtRequestFilter jwtRequestFilter;

    /**
     * Configures authentication mechanism by providing password encoder and user details service. 
     * 
     * @param auth AuthenticationManagerBuilder Builder to set the authentication mechanism.
     * @throws Exception Thrown if setting authentication mechanism failed.
     */
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		// configure AuthenticationManager so that it knows from where to load
		// user for matching credentials
		// Use BCryptPasswordEncoder
		auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
	}

    /**
     * Returns password encoder to be used for authentication and encoded credentials involved.
     * 
     * @return PasswordEncoder Password encoder to be used for authentication and encoded credentials involved.
     */
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

    /**
     * Returns authentication manager that is doing the authentication.
     * 
     * @return AuthenticationManager Authentication manager that does the authentication.
     * @throws Exception Thrown if authentication failed.
     */
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

    /**
     * Configures HTTP security by setting permitted requests and requests that need authentication.
     * 
     * @param httpSecurity HttpSecurity HTTP security that can be used to configure what is permitted and what not.
     * @throws Exception Thrown if configuration of HTTP security failed.
     */
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable()
                    .authorizeRequests()
                    .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                    .antMatchers(HttpMethod.POST, "/restapi/auth/signup", "/restapi/auth/signin").permitAll()
                    .antMatchers(HttpMethod.GET, "/", "/**/favicon.ico", "/**/*.{js,html,css}").permitAll()
                    .anyRequest().authenticated()
                    .and()
                    .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
                    .and()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        
		httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
	}
    
}
