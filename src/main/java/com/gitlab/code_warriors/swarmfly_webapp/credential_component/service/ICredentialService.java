package com.gitlab.code_warriors.swarmfly_webapp.credential_component.service;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.gitlab.code_warriors.swarmfly_webapp.credential_component.entity.CredentialEntity;
import com.gitlab.code_warriors.swarmfly_webapp.user_component.entity.UserEntity;
import com.gitlab.code_warriors.swarmfly_webapp.user_component.exceptions.UserNotFoundException;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 * Service interface to load credentials by user data.
 * 
 * @author christian.hueser
 */
@Service
@Transactional
public interface ICredentialService {
    
    /**
     * Returns credential entity by user ID of the related user entity.
     * 
     * @param userId User ID of the user entity that is related to the credential entity.
     * @return CredentialEntity Credential entity that is related to the user ID of a user entity.
     * @throws UserNotFoundException Thrown if user can not be found by user ID given. 
     */
    public Optional<CredentialEntity> getCredentialByUserId(Long userId) throws UserNotFoundException;

    /**
     * Returns credential entity by username of the related user entity.
     * 
     * @param username String Username of the user entity that is related to the credential entity.
     * @return CredentialEntity Credential entity that is related to the user ID of a user entity.
     * @throws UserNotFoundException Thrown if user can not be found by username given. 
     */
    public Optional<CredentialEntity> getCredentialByUsername(String username) throws UserNotFoundException;
    
    /**
     * Returns credential entity by username of the related user entity.
     * 
     * @param email String Email of the user entity that is related to the credential entity.
     * @return CredentialEntity Credential entity that is related to the user ID of a user entity.
     * @throws UserNotFoundException Thrown if user can not be found by email given. 
     */
    public Optional<CredentialEntity> getCredentialByEmail(String email) throws UserNotFoundException;
    
    /**
     * Saves user credential entity.
     * 
     * @param credential CredentialEntity Credential entity associated with a user to be saved.
     * @return CredentialEntity Saved credential entity associated with a user.
     */
    public CredentialEntity saveCredential(CredentialEntity credential);
    
}
