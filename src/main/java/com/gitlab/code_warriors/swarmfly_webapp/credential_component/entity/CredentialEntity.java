package com.gitlab.code_warriors.swarmfly_webapp.credential_component.entity;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.gitlab.code_warriors.swarmfly_webapp.user_component.entity.*;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Entity class that models credentials.
 * 
 * @author christian.hueser
 */
@Entity
@Table(name = "credentials")
public class CredentialEntity implements Serializable {

    /**
     * ID of a credential entity.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "credential_id")
    private Long id;

    /**
     * Password of User.
     */
    @Column(name = "password")
    private String password;
    
    
    /**
     * Relationship to user with foreign key.
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private UserEntity user;

    /**
     * Returns an ID of a credential entity.
     * 
     * @return Long ID of a credential entity.
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the ID of a credential entity.
     * 
     * @param id Long ID of a credential entity.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns password of a user.
     * 
     * @return String Password of a user.
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Sets the password of a user.
     * 
     * @param password String Password of a user.
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * Returns a user entity related to a credential entity.
     * 
     * @return UserEntity User entity related to a credential entity.
     */
    public UserEntity getUser() {
        return this.user;
    }

    /**
     * Sets the user entity related to a credential entity.
     * 
     * @param user User entity related to a credential entity.
     */
    public void setUser(UserEntity user) {
        this.user = user;
    }

}
