package com.gitlab.code_warriors.swarmfly_webapp.credential_component.service;

/*
 * SPDX-FileCopyrightText: 2022 Code Warriors
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Swarmfly Web - A Group Activity Coordination Management Webapp
 * 
 * Copyright (C) 2022 Code Warriors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.gitlab.code_warriors.swarmfly_webapp.credential_component.entity.CredentialEntity;
import com.gitlab.code_warriors.swarmfly_webapp.credential_component.repository.ICredentialRepository;
import com.gitlab.code_warriors.swarmfly_webapp.user_component.entity.UserEntity;
import com.gitlab.code_warriors.swarmfly_webapp.user_component.exceptions.UserNotFoundException;
import com.gitlab.code_warriors.swarmfly_webapp.user_component.repository.IUserRepository;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class to load credentials by user data.
 * 
 * @author christian.hueser
 */
@Service
@Transactional
public class CredentialService implements ICredentialService {
	
    /**
     * Credential repository auto-wired into service.
     */
    @Autowired
    private ICredentialRepository credentialRepository;

    /**
     * User repository auto-wired into service.
     */
    @Autowired
    private IUserRepository userRepository;
    
    /**
     * Returns credential entity by user ID of the related user entity.
     * 
     * @param userId User ID of the user entity that is related to the credential entity.
     * @return CredentialEntity Credential entity that is related to the user ID of a user entity.
     * @throws UserNotFoundException Thrown if user can not be found by user ID given. 
     */
    @Override
    public Optional<CredentialEntity> getCredentialByUserId(Long userId) throws UserNotFoundException {
        Optional<UserEntity> user = userRepository.findById(userId);
        if (user.isPresent()) {
            Optional<CredentialEntity> credential = credentialRepository.findByUserId(user.get().getId());
            return credential;
        }
        throw new UserNotFoundException("Credentials of user can not be found by the user ID given.");
    }
    
    /**
     * Returns credential entity by username of the related user entity.
     * 
     * @param username String Username of the user entity that is related to the credential entity.
     * @return CredentialEntity Credential entity that is related to the user ID of a user entity.
     * @throws UserNotFoundException Thrown if user can not be found by username given. 
     */
    @Override
    public Optional<CredentialEntity> getCredentialByUsername(String username) throws UserNotFoundException {
        Optional<UserEntity> user = userRepository.findByUsername(username);
        if (user.isPresent()) {
            Optional<CredentialEntity> credential = credentialRepository.findByUserId(user.get().getId());
            return credential;
        }
        throw new UserNotFoundException("Credentials of user can not be found by the username given.");
    }

    /**
     * Returns credential entity by username of the related user entity.
     * 
     * @param email String Email of the user entity that is related to the credential entity.
     * @return CredentialEntity Credential entity that is related to the user ID of a user entity.
     * @throws UserNotFoundException Thrown if user can not be found by email given. 
     */
    @Override
    public Optional<CredentialEntity> getCredentialByEmail(String email) throws UserNotFoundException {
        Optional<UserEntity> user = userRepository.findByEmail(email);
        if (user.isPresent()) {
            Optional<CredentialEntity> credential = credentialRepository.findByUserId(user.get().getId());
            return credential;
        }
        throw new UserNotFoundException("Credentials of user can not be found by the email given.");
    }

    /**
     * Saves user credential entity.
     * 
     * @param credential CredentialEntity Credential entity associated with a user to be saved.
     * @return CredentialEntity Saved credential entity associated with a user.
     */
    @Override
    public CredentialEntity saveCredential(CredentialEntity credential) {
        
        CredentialEntity credentialEntity = credentialRepository.save(credential);
        return credentialEntity;
        
    }
    
}
