<!--
SPDX-FileCopyrightText: 2022 Code Warriors

SPDX-License-Identifier: AGPL-3.0-only
-->

<!--
Swarmfly Web - A Group Activity Coordination Management Webapp

Copyright (C) 2022 Code Warriors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-->

# Swarmfly Webapp

## Setup Project on Ubuntu

### Install Java

[Download](https://www.oracle.com/java/technologies/downloads/)

```shell
cd ~/Downloads/
sudo apt install jdk-17_linux-x64_bin.deb
```

[Documentation to configure Alternative Systems](https://wiki.ubuntuusers.de/Alternativen-System/)

```shell
sudo update-alternatives --install /usr/bin/java java /usr/lib/jvm/jdk-17/bin/java 1
sudo update-alternatives --install /usr/bin/javac javac /usr/lib/jvm/jdk-17/bin/javac 1
sudo update-alternatives --config java
sudo update-alternatives --config javac
```

### Install Maven

1. [Download Maven](https://maven.apache.org/download.cgi)
2. [Install Maven](https://maven.apache.org/install.html)

```shell
sudo cp apache-maven-3.8.4-bin.tar.gz /usr/local/
cd /usr/local/
sudo tar xzvf apache-maven-3.8.4-bin.tar.gz
```

### Edit file ~/.profile

```shell
vim ~/.profile
```

```
export JAVA_HOME=/usr/lib/jvm/jdk-17
export MAVEN_HOME=/usr/local/apache-maven-3.8.4
export CATALINA_HOME=/opt/tomcat
export CATALINA_BASE=${CATALINA_HOME}
export PATH="${JAVA_HOME}/bin:${MAVEN_HOME}/bin:${CATALINA_HOME}/bin:${PATH}"
```

```shell
source ~/.profile
```

### Install Git

#### Latest

[Instructions](https://git-scm.com/download/linux)

```shell
sudo add-apt-repository ppa:git-core/ppa
sudo apt update
sudo apt install git
```

#### Configure Git

```shell
git config --global user.name "Christian Hüser"
git config --global user.email "c_hueser@web.de"
git config --global core.editor vim
git config --global --list
```

### Clone Project

```shell
git clone https://gitlab.com/code_warriors/swarmfly_webapp.git
```

### Install NetBeans

[Download](https://netbeans.apache.org/download/index.html)

```shell
sudo bash Apache-NetBeans-13-bin-linux-x64.sh
```

### Open Project in NetBeans and Resolve Project Problems

1. Start NetBeans
2. Open Project
3. Choose project folder
4. Resolve Project Problems (this will download all project dependencies which
could take some time)

### Install and Run MySQL Database Server

#### Install MySQL Database Server

[Download](https://dev.mysql.com/downloads/)

Apt Config: [Download](https://dev.mysql.com/downloads/repo/apt/)

Optionally:

- Server: [Download](https://dev.mysql.com/downloads/mysql/)
- Shell: [Download](https://dev.mysql.com/downloads/shell/)
- Workbench: [Download](https://dev.mysql.com/downloads/workbench/)

```shell
cd ~/Downloads/
sudo apt install mysql-apt-config_0.8.19-1_all.deb
sudo apt update
sudo apt install mysql-common
sudo apt install mysql-community-server
sudo apt install mysql-community-client
sudo apt install mysql-shell
sudo apt install mysql-workbench-community
sudo apt install mysql-connector-java
```

```shell
sudo systemctl status mysql.service
sudo mysql_secure_installation
```

#### Create MySQL User and Database

1. Start MySQL Server Service
```shell
sudo systemctl restart mysql.service
```
2. Open MySQL client:
```shell
sudo mysql -u root -p
```
```SQL
CREATE USER 'swarmfly_db_user'@'localhost' IDENTIFIED BY 'ChangeMe_1x';
GRANT ALL PRIVILEGES ON *.* TO 'swarmfly_db_user'@'localhost';
FLUSH PRIVILEGES;
```
```shell
exit
```
3. Open MySQL Workbench
4. Edit connection
5. Change user to `swarmfly_db_user`
6. Test connection
7. Open MySQL client:
```shell
sudo mysql -u root -p
```
```SQL
CREATE DATABASE swarmfly_db CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
```
```shell
exit
```
8. Open MySQL Workbench
9. Edit Connection
10. Default Schema: swarmfly_db
11. Test connection
12. Connect to Database

### Configure MySQL Database Server in Netbeans

#### Create Database Service

1. Open Services Tab
2. Right-Click on Databases
3. Choose New Connection
4. Choose MySQL Driver and add Driver File
Driver: `/usr/share/java/mysql-connector-java-8.0.27.jar`
5. Customize Connection:
  - driver: MySQL
  - database: swarmfly_db
  - host: localhost
  - port: 3306
  - username: swarmfly_db_user
  - password: ChangeMe_1x
  - jdbc url: 
```
jdbc:mysql://localhost:3306/swarmfly_db?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=Europe/Berlin
```
6. Test connection
7. Choose Schema Default
8. Choose Connection Name

#### Execute Database Migrations

```shell
mvn flyway:migrate
```

### Install Tomcat

[Download](https://tomcat.apache.org/download-90.cgi)

1. Install and configure Tomcat
```shell
sudo cp apache-tomcat-9.0.55.tar.gz /opt/
cd /opt/
sudo tar xzvf apache-tomcat-9.0.55.tar.gz
sudo mv apache-tomcat-9.0.55/ tomcat/
sudo useradd -m -U -d /opt/tomcat -s /bin/false tomcat
sudo chown -R tomcat:christianhueser /opt/tomcat/
sudo chmod -R 0775 /opt/tomcat/
```
2. Create systemd service file
```shell
sudo vim /etc/systed/system/tomcat.service
```
```
[Unit]
Description=Tomcat 9 servlet container
After=network.target
 
[Service]
Type=forking
 
User=tomcat
Group=tomcat
 
Environment="CATALINA_PID=/opt/tomcat/temp/tomcat.pid"
 
ExecStart=/opt/tomcat/bin/startup.sh
ExecStop=/opt/tomcat/bin/shutdown.sh
 
[Install]
WantedBy=multi-user.target
```
3. Edit `/opt/tomcat/conf/tomcat-users.xml`:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<tomcat-users xmlns="http://tomcat.apache.org/xml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.0" xsi:schemaLocation="http://tomcat.apache.org/xml tomcat-users.xsd">
    <role rolename="manager-script"/>
    <role rolename="manager-gui"/>
    <role rolename="manager-jmx"/>
    <role rolename="manager-status"/>
    <role rolename="admin-gui"/>
    <role rolename="admin-script"/>
    <role rolename="admin"/>
    <role rolename="manager"/>
    <user password="ChangeMe_1x" roles="manager-script,manager-gui,manager-jmx,manager-status,admin-gui,admin-script,admin,manager" username="tomcat"/>
</tomcat-users>
```
4. Start service
```shell
sudo systemctl daemon-reload
sudo systemctl enable tomcat.service
sudo systemctl start tomcat.service
```

### Configure Tomcat Server in Netbeans

#### Create Webserver Service

1. Open Services Tab
2. Right-Click on Servers
3. Choose Add Server
4. Choose Apache Tomcat
5. Choose Server Location: /opt/tomcat/
6. Enter Username and Password
7. Untick Create user if it does not exist
8. Click Finish

### Run Project

#### Edit Project Properties

1. Set project to Default Project: Menu > Run > Set Main Project: SwarmflyWebapp
2. Choose Menu > File > Project Properties
3. Choose Sources: Sources/Binaries Format: 17
4. Build / Compile: Java Platform: JDK17
  - Manage Java Platform
  - Add Java Platform
  - Choose Java Standard Edition
  - Choose Path
  - Choose Name
5. Choose Categories: Run
6. Choose Server: Apache Tomcat

#### Build and Run

1. Click Run > Clean and Build Main Project
2. Click Run > Run Main Project
3. Open Browser: http://localhost:8080/

## Upgrade Services on Ubuntu

### Upgrade Tomcat 9 Service

[Download](https://tomcat.apache.org/download-90.cgi)

```shell
sudo cp apache-tomcat-9.0.55.tar.gz /opt/
cd /opt/
sudo tar xzvf apache-tomcat-9.0.55.tar.gz
sudo mv tomcat/ tomcat_9_0_54/
sudo mv apache-tomcat-9.0.55/ tomcat/
sudo cp tomcat_9_0_54/conf/tomcat_users.xml tomcat/conf/tomcat_users.xml
sudo chown -R tomcat:christianhueser /opt/tomcat/
sudo systemctl restart tomcat.service
```

## Setup Project on Windows

### Install Java

[Download](https://www.oracle.com/java/technologies/downloads/)

### Install Maven

1. [Download Maven](https://maven.apache.org/download.cgi)
2. [Install Maven](https://maven.apache.org/install.html)

### Edit Environment Variables

- New JAVA_HOME System Variable: C:\Program Files\Java\jdk-17.0.1
- New MAVEN_HOME System Variable: C:\Program Files\apache-maven-3.8.4
- Add to PATH System Variable: %JAVA_HOME%\bin
- Add to PATH System Variable: %MAVEN_HOME%\bin

### Install Git

#### Latest

[Download](https://gitforwindows.org/)

#### Configure Git

```shell
git config --global user.name "Christian Hüser"
git config --global user.email "c_hueser@web.de"
git config --global core.editor vim
git config --global --list
```

### Clone Project

```shell
git clone https://gitlab.com/code_warriors/swarmfly_webapp.git
```

### Install NetBeans

[Download](https://netbeans.apache.org/download/index.html)

### Open Project in NetBeans and Resolve Project Problems

1. Start NetBeans
2. Open Project
3. Choose project folder
4. Resolve Project Problems (this will download all project dependencies which
could take some time)

### Install and Run MySQL Database Server

### Install Visual Studio

[Download](https://visualstudio.microsoft.com/de/vs/)

#### Install Python

[Download](python.org/downloads/windows/)

#### MySQL Connectror Python

[Download](https://dev.mysql.com/downloads/connector/python/)

#### Install MySQL for Visual Studio and MySQL Installer for Windows

[Download](https://dev.mysql.com/downloads/)

#### Install MySQL Connector Java

[Download](https://dev.mysql.com/downloads/connector/j/)

#### Create MySQL Database

1. Open MySQL Workbench
2. Connect to Database
3. Execute sql statement
```SQL
CREATE DATABASE swarmfly_db CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
```

### Configure MySQL Database Server in Netbeans

#### Create Database Service

1. Open Services Tab
2. Right-Click on Databases
3. Choose New Connection
4. Choose MySQL Driver and add Driver File
Driver: `C:\Users\chris\.m2\repository\mysql\mysql-connector-java\8.0.27\mysql-connector-java-8.0.27.jar`
5. Customize Connection:
  - driver: MySQL
  - database: swarmfly_db
  - host: localhost
  - port: 3306
  - username: swarmfly_db_user
  - password: ChangeMe_1x
  - jdbc url: 
```
jdbc:mysql://localhost:3306/swarmfly_db?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=Europe/Berlin
```
6. Test connection
7. Choose Schema Default
8. Choose Connection Name

#### Execute Database Migrations

```shell
mvn flyway:migrate
```

### Install Tomcat

[Download](https://tomcat.apache.org/download-90.cgi)

Edit `C:\Program Files\Apache Software Foundation\Tomcat 9.0\conf\tomcat-users.xml`:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<tomcat-users xmlns="http://tomcat.apache.org/xml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.0" xsi:schemaLocation="http://tomcat.apache.org/xml tomcat-users.xsd">
    <role rolename="manager-script"/>
    <role rolename="manager-gui"/>
    <role rolename="manager-jmx"/>
    <role rolename="manager-status"/>
    <role rolename="admin-gui"/>
    <role rolename="admin-script"/>
    <role rolename="admin"/>
    <role rolename="manager"/>
    <user password="ChangeMe_1x" roles="manager-script,manager-gui,manager-jmx,manager-status,admin-gui,admin-script,admin,manager" username="tomcat"/>
</tomcat-users>
```

### Configure Windows Service for Tomcat

1. Open Windows Services
2. Open properties of Tomcat service
3. Open tab "Log On"
4. Choose "Log on as: Local System account"
5. Stop / Start Service

### Configure Tomcat Server in Netbeans

#### Create Webserver Service

1. Open Services Tab
2. Right-Click on Servers
3. Choose Add Server
4. Choose Apache Tomcat
5. Choose Server Location: `C:\Program Files\Apache Software Foundation\Tomcat 9.0\`
6. Enter Username and Password
7. Untick Create user if it does not exist
8. Click Finish

### Run Project

#### Edit Project Properties

1. Set project to Default Project: Menu > Run > Set Main Project: SwarmflyWebapp
2. Choose Menu > File > Project Properties
3. Choose Sources: Sources/Binaries Format: 17
4. Build / Compile: Java Platform: JDK17
  - Manage Java Platform
  - Add Java Platform
  - Choose Java Standard Edition
  - Choose Path
  - Choose Name
4. Choose Categories: Run
5. Choose Server: Apache Tomcat

#### Edit Options

1. Choose tab Java
2. Choose tab Maven
3. Browse Maven installation: `C:\Program Files\apache-maven-3.8.4`

#### Build and Run

1. Click Run > Clean and Build Main Project
2. Click Run > Run Main Project
3. Open Browser: http://localhost:8080/

## Upgrade Services on Windows

### Upgrade Tomcat 9 Service

[Download](https://tomcat.apache.org/download-90.cgi)

Copy `C:\Program Files\Apache Software Foundation\Tomcat 9.0.54\conf\tomcat-users.xml` to `C:\Program Files\Apache Software Foundation\Tomcat 9.0.55\conf\tomcat-users.xml`:

#### Configure Windows Service for Tomcat

1. Open Windows Services
2. Disable previous Tomcat service
3. Open properties of new Tomcat service
4. Open tab "Log On"
5. Choose "Log on as: Local System account"
6. Stop / Start Service
7. Open CMD with administrator rights
8. Delete old Windows service: `sc delete Tomcat9_0_54`
