# License Hint

Copyright © 2022 Code Warriors

This work is licensed under the [AGPL-3.0-only](LICENSESAGPL-3.0-only.txt) license.

Please see the individual files for more accurate information.

> **Hint:** We provided the copyright and license information in accordance to the [REUSE Specification 3.0](https://reuse.software/spec/).
